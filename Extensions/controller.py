__author__ = 'beastarman'

from math import floor,ceil

class InconsistentStatusException(Exception):
	@staticmethod
	def create_from_states(expected,found):
		return InconsistentStatusException("Status at %s. Expected %s" % (found, ",".join([str(i) for i in expected])))

	@staticmethod
	def create_from_states_not(expected,found):
		return InconsistentStatusException("Status at %s. Blocking %s" % (found, ",".join([str(i) for i in expected])))

class Controller(object):
	status = None
	request = None

	def __init__(self,request,*args, **kwargs):
		self.request = request
		self.status = self._create(*args,**kwargs)

	def _create(self):
		return 0

	def assertStatus(self,*status):
		for s in status:
			if((s & self.status) > 0):
				return
		raise InconsistentStatusException.create_from_states(status,self.status)

	def desertStatus(self,*status):
		for s in status:
			if((s & self.status) == 0):
				return
		raise InconsistentStatusException.create_from_states_not(status,self.status)

	@staticmethod
	def can(request):
		return True

class SearchController(Controller):
	_query = None
	allowed_results_per_page = [5,20,100]

	Controller = None

	def set_query(self,query):
		self._query = query

	def sliced(self,a=None,b=None):
		return [self.Controller(self.request,i) for i in self._query[a:b]]

	def __getitem__(self,key):
		return self.Controller(self.request,self._query[key])

	def __iter__(self):
		list(self._query)
		return iter([self.Controller(self.request,i) for i in self._query])

	def __len__(self):
		return self._query.count()

class SearchPaginator(object):
	def __init__(self,search_controller,offset=0,results_per_page=0):
		try:
			offset = max(0,int(offset))
		except Exception:
			offset=0

		try:
			results_per_page = int(results_per_page)
		except:
			results_per_page=0

		if(results_per_page >= search_controller.allowed_results_per_page[-1]):
			results_per_page = search_controller.allowed_results_per_page[-1]
		else:
			for i in search_controller.allowed_results_per_page:
				if(results_per_page <= i):
					results_per_page = i
					break

		offset = results_per_page*(offset/results_per_page)

		self._total_results = len(search_controller)

		objects = search_controller.sliced(offset,offset+results_per_page+1)

		self._has_next_page = (len(objects) > results_per_page)
		self._has_prev_page = (offset >= results_per_page)

		self._objects = objects[:results_per_page]
		self._prev_offset = offset - results_per_page if self._has_prev_page else None
		self._next_offset = offset + results_per_page if self._has_next_page else None
		self._cur_offset = offset
		self._results_per_page = results_per_page

	def __getitem__(self,key):
		return self._objects[key]

	def __iter__(self):
		return iter(self._objects)

	def __len__(self):
		return len(self._objects)

	def offset(self):
		return self._cur_offset

	def results_per_page(self):
		return self._results_per_page

	def has_next_page(self):
		return self._has_next_page

	def has_prev_page(self):
		return self._has_prev_page

	def prev_offset(self):
		return self._prev_offset

	def next_offset(self):
		return self._next_offset

	def total_results(self):
		return self._total_results

	def cur_page(self):
		return int(floor(float(self._cur_offset)/self._results_per_page))

	def total_pages(self):
		return int(ceil(float(self._total_results)/self._results_per_page))

class SearchPaginatorBrowser(object):
	def __init__(self, search_paginator, pages_start=1, pages_around=3, pages_end=1, first_page=1):
		self.search_paginator = search_paginator
		self.pages_start = pages_start
		self.pages_around = pages_around
		self.pages_end = pages_end
		self.first_page = first_page

	def get_pages(self):
		if(not len(self.search_paginator)):
			return []

		start = [i for i in range(self.pages_start) if i>=0 and i<self.search_paginator.total_pages()]
		middle = [i for i in range(self.search_paginator.cur_page() - self.pages_around, self.search_paginator.cur_page() + self.pages_around + 1) if i>=0 and i<self.search_paginator.total_pages()]
		end = [i for i in range(self.search_paginator.total_pages() - self.pages_end, self.search_paginator.total_pages()) if i>=0 and i<self.search_paginator.total_pages()]

		all_pages = sorted([{
			'page': i + self.first_page,
			'offset': i * self.search_paginator.results_per_page(),
		} for i in list(set(start+middle+end))])

		pages = [all_pages[:1]]

		for i in all_pages[1:]:
			if(i['page'] == pages[-1][-1]['page']+1):
				pages[-1].append(i)
			else:
				pages.append([i])

		return pages

def assertStatus(*status):
	def assertStatusInner(func):
		def inner(self,*args,**kwargs):
			self.assertStatus(*status)
			return func(self,*args,**kwargs)
		return inner
	return assertStatusInner;

def desertStatus(*status):
	def assertStatusInner(func):
		def inner(self,*args,**kwargs):
			self.desertStatus(*status)
			return func(self,*args,**kwargs)
		return inner
	return assertStatusInner;
