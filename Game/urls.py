__author__ = 'beastarman'

from django.conf.urls import patterns, include, url

urlpatterns = patterns('Game.views',
    url(r'^game/', "game"),
    url(r'^scene/', "scene"),
    url(r'^advance/', "advance"),
)
