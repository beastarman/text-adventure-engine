__author__ = 'beastarman'

from Extensions.controller import *
from Game.models import *

class LoggedInController(Controller):
	STATUS_LOGGED_IN = 1
	STATUS_NOT_LOGGED_IN = 0

	_user = None

	def _create(self):
		if(not self.request.user.is_authenticated()):
			return self.STATUS_NOT_LOGGED_IN

		try:
			self._user = self.request.user.gameuser
		except GameUser.DoesNotExist:
			return self.STATUS_NOT_LOGGED_IN

	@property
	@assertStatus(STATUS_LOGGED_IN)
	def character(self):
		return CharacterController(self.request,self._user.character)

class CharacterController(Controller):
	STATUS_OK = 1;

	_character = None;

	def _create(self,characterModel):
		self._character = characterModel

		return self.STATUS_OK

	@property
	@assertStatus(STATUS_OK)
	def maxHP(self):
		return self._character.maxHP

	@property
	@assertStatus(STATUS_OK)
	def hp(self):
		return self._character.hp

	@property
	@assertStatus(STATUS_OK)
	def hpRatio(self):
		return float(self._character.hp)/self._character.maxHP;

	@property
	@assertStatus(STATUS_OK)
	def maxLust(self):
		return self._character.maxLust

	@property
	@assertStatus(STATUS_OK)
	def lust(self):
		return self._character.lust

	@property
	@assertStatus(STATUS_OK)
	def lustRatio(self):
		return float(self._character.lust)/self._character.maxLust;

	@property
	@assertStatus(STATUS_OK)
	def name(self):
		return self._character.name;

	@assertStatus(STATUS_OK)
	def getAttack(self):
		return self._character.attack;

	@assertStatus(STATUS_OK)
	def asDict(self):
		return\
		{
			"hp":
				{
					"current": self.hp,
					"max": self.maxHP,
					"ratio": self.hpRatio,
				},
			"lust":
				{
					"current": self.lust,
					"max": self.maxLust,
					"ratio": self.lustRatio,
				},
			"name": self.name,
		}

class SceneController(LoggedInController):
	def _create(self):
		ret = super(SceneController,self)._create();

		if(ret == self.STATUS_NOT_LOGGED_IN):
			return ret;

		self._character = self._user.character
		self._scene = self._user.state

		return self.STATUS_LOGGED_IN

	@assertStatus(LoggedInController.STATUS_LOGGED_IN)
	def getText(self):
		return self._scene.getText(self._user)

	def parseText(self,text):
		return [i.strip() for i in text.split("\n") if len(i.strip())>0]

	@assertStatus(LoggedInController.STATUS_LOGGED_IN)
	def getOptions(self):
		return [{
				"name": i.name,
				"key": i.hash_id,
				"help": i.help
			 } for i in self._scene.targets.all() if i.getSpecialized().testOption(self._user)]

	@assertStatus(LoggedInController.STATUS_LOGGED_IN)
	def asJSON(self):
		return {
			"text": self.parseText(self.getText()),
			"options": self.getOptions(),
			"character": self.character.asDict()
		}

	@assertStatus(LoggedInController.STATUS_LOGGED_IN)
	def getSpecialized(self):
		try:
			self._scene.battlestate
			return BattleSceneController(self.request)
		except:
			return self

class AdvanceSceneController(SceneController):
	def _create(self,advance_key):
		ret = super(AdvanceSceneController,self)._create();

		if(ret == self.STATUS_NOT_LOGGED_IN):
			return ret;

		self._advance_key = advance_key

		return ret

	@assertStatus(LoggedInController.STATUS_LOGGED_IN)
	def advance(self):
		target = self._scene.targets.filter(hash_id = self._advance_key)

		target = target[0] if len(target) else None

		if(not target):
			return None

		target = target.getSpecialized()

		if(not target.testOption(self._user)):
			return None

		target.execTask(self._user);

		targetScene = target.getTarget(self._user)

		if(self._user.lastHub_id != targetScene.pk and targetScene.tags.filter(tag=GlobalSettings.objects.filter(key=GlobalSettings.HUB_WORLD_TAG)[0].value).exists()):
			self._user.lastHub = targetScene

		self._scene = targetScene
		self._user.state = self._scene
		self._user.state.execTask(self._user)
		self._user.character.save()
		self._user.save()

	@assertStatus(LoggedInController.STATUS_LOGGED_IN)
	def getSpecialized(self):
		try:
			print "1A", self._scene
			print "2A",self._scene.battlestate
			self._scene.battlestate
			return AdvanceBattleSceneController(self.request,self._advance_key)
		except Exception as e:
			return self

class EnemyCharacterController(CharacterController):
	@assertStatus(LoggedInController.STATUS_LOGGED_IN)
	def getRandomCharacterAction(self,gameuser,target):
		return self._character.getRandomCharacterAction(gameuser,gameuser.character,target)

class BattleSceneController(SceneController):
	STATUS_NOT_BATTLE_SCENE = 4

	ATTACK_KEY = "BattleOption_ATTACK"
	DEFEND_KEY = "BattleOption_DEFEND"
	FINISH_KEY = "BattleOption_FINISH"

	ATTACK = ("Attack",ATTACK_KEY,"Attack enemy directly")
	DEFEND = ("Defend",DEFEND_KEY,"Defend yourself from enemy actions")
	FINISH = ("Next",FINISH_KEY,"")

	def _create(self):
		ret = super(BattleSceneController,self)._create();

		if(ret == self.STATUS_NOT_LOGGED_IN):
			return ret;

		try:
			self._battleState = self._scene.battlestate
		except Exception as e:
			return self.STATUS_NOT_BATTLE_SCENE

		return self.STATUS_LOGGED_IN

	@assertStatus(LoggedInController.STATUS_LOGGED_IN)
	def getEnemy(self):
		if(not self._user.enemyState):
			self._user.enemyState = EnemyState.objects.create(character = self._battleState.enemy,hp = self._battleState.enemy.hp,lust = self._battleState.enemy.lust)
			self._user.save()

		enemy = self._battleState.enemy
		enemy.hp = self._user.enemyState.hp
		enemy.lust = self._user.enemyState.lust

		return EnemyCharacterController(self.request,self._battleState.enemy)

	@assertStatus(LoggedInController.STATUS_LOGGED_IN)
	def getOptions(self):
		return [{
				"name": i[0],
				"key": i[1],
				"help": i[2],
			 } for i in [self.ATTACK,self.DEFEND] if(self.getEnemy().hp > 0 and self._user.character.hp > 0)] + super(BattleSceneController, self).getOptions()

	@assertStatus(LoggedInController.STATUS_LOGGED_IN)
	def getText(self):
		if(self._user.character.hp <= 0):
			return "You was defeated"
		elif(self.getEnemy().hp <= 0):
			return "You defeated %s" % self.getEnemy().name
		else:
			ret = super(BattleSceneController, self).getText()

			if(self._user.enemyState.lastAction):
				return "%s\n\n%s" % (self._user.enemyState.lastAction.getDescription(self._user,self._battleState.enemy,self._user.character),ret)
			else:
				return ret

	@assertStatus(LoggedInController.STATUS_LOGGED_IN)
	def asJSON(self):
		return {
			"text": self.parseText(self.getText()),
			"options": self.getOptions(),
			"character": self.character.asDict(),
			"extra": {
				"type": "battle",
				"enemy": self.getEnemy().asDict(),
			}
		}

class AdvanceBattleSceneController(BattleSceneController):
	def _create(self,advance_key):
		self._advance_key = advance_key

		return super(AdvanceBattleSceneController, self)._create()

	@assertStatus(LoggedInController.STATUS_LOGGED_IN)
	def advance(self):
		if(self._advance_key == self.ATTACK_KEY):
			self._user.enemyState.hp = max(0,self._user.enemyState.hp-self.character.getAttack())
			self._user.enemyState.save()

			self._scene = self._battleState
		elif(self._advance_key == self.DEFEND_KEY):
			self._scene = self._battleState
		else:
			target = self._scene.targets.filter(hash_id = self._advance_key)

			target = target[0] if len(target) else None

			if(not target):
				return None

			target = target.getSpecialized()

			if(not target.testOption(self._user)):
				return None

			self._user.enemyState.delete()
			self._user.enemyState = None
			self._user.save()

			self._scene = target.getTarget(self._user)

			self._scene.execTask(self._user);

			self._user.state = self._scene
			self._user.state.execTask(self._user)
			self._user.character.save()
			self._user.save()

			return

		self._scene.execTask(self._user);

		self._user.state = self._scene
		self._user.state.execTask(self._user)
		self._user.character.save()
		self._user.save()

		if(self.getEnemy().hp <= 0):
			return;

		self._user.enemyState.lastAction = self.getEnemy().getRandomCharacterAction(self._user,self._user.character)
		self._user.enemyState.lastAction.execTask(self._user,self._battleState.enemy,self._user.character)

		self._user.character.save()
		self._user.enemyState.save()