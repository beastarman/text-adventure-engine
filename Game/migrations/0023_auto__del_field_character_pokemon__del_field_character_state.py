# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Character.pokemon'
        db.delete_column(u'Game_character', 'pokemon_id')

        # Deleting field 'Character.state'
        db.delete_column(u'Game_character', 'state_id')

        # Removing M2M table for field events on 'Character'
        db.delete_table(db.shorten_name(u'Game_character_events'))


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Character.pokemon'
        raise RuntimeError("Cannot reverse this migration. 'Character.pokemon' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Character.pokemon'
        db.add_column(u'Game_character', 'pokemon',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Game.Pokemon']),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Character.state'
        raise RuntimeError("Cannot reverse this migration. 'Character.state' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Character.state'
        db.add_column(u'Game_character', 'state',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Game.State']),
                      keep_default=False)

        # Adding M2M table for field events on 'Character'
        m2m_table_name = db.shorten_name(u'Game_character_events')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('character', models.ForeignKey(orm[u'Game.character'], null=False)),
            ('event', models.ForeignKey(orm[u'Game.event'], null=False))
        ))
        db.create_unique(m2m_table_name, ['character_id', 'event_id'])


    models = {
        u'Game.battlestate': {
            'Meta': {'object_name': 'BattleState', '_ormbases': [u'Game.State']},
            'enemy': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Game.Character']"}),
            u'state_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Game.State']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'Game.character': {
            'Meta': {'object_name': 'Character'},
            'attack': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'defense': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'hp': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lust': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'maxHP': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'maxLust': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'Game.event': {
            'Meta': {'object_name': 'Event'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '16', 'primary_key': 'True'})
        },
        u'Game.gameuser': {
            'Meta': {'object_name': 'GameUser'},
            'character': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Game.Character']"}),
            'events': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['Game.Event']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pokemon': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Game.Pokemon']"}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Game.State']"}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Game.User']", 'unique': 'True'})
        },
        u'Game.pokemon': {
            'Meta': {'object_name': 'Pokemon'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pokedex_id': ('django.db.models.fields.IntegerField', [], {})
        },
        u'Game.randomstateoption': {
            'Meta': {'object_name': 'RandomStateOption', '_ormbases': [u'Game.StateOptionBase']},
            u'stateoptionbase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Game.StateOptionBase']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'Game.randomstateoptionslice': {
            'Meta': {'object_name': 'RandomStateOptionSlice'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'random_scene_option': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Game.RandomStateOption']"}),
            'slice': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'target': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'random_sources'", 'to': u"orm['Game.State']"}),
            'test': ('django.db.models.fields.CharField', [], {'default': "'True'", 'max_length': '512'})
        },
        u'Game.state': {
            'Meta': {'object_name': 'State'},
            'arrivalExec': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['Game.Tag']", 'symmetrical': 'False', 'blank': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        u'Game.stateoption': {
            'Meta': {'object_name': 'StateOption', '_ormbases': [u'Game.StateOptionBase']},
            u'stateoptionbase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Game.StateOptionBase']", 'unique': 'True', 'primary_key': 'True'}),
            'target': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'sources'", 'to': u"orm['Game.State']"}),
            'transactionExec': ('django.db.models.fields.TextField', [], {'blank': 'True'})
        },
        u'Game.stateoptionbase': {
            'Meta': {'object_name': 'StateOptionBase'},
            'hash_id': ('django.db.models.fields.CharField', [], {'max_length': '64', 'primary_key': 'True'}),
            'help': ('django.db.models.fields.TextField', [], {'max_length': '128', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'source': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'targets'", 'to': u"orm['Game.State']"}),
            'test': ('django.db.models.fields.CharField', [], {'default': "'True'", 'max_length': '512'})
        },
        u'Game.tag': {
            'Meta': {'object_name': 'Tag'},
            'tag': ('django.db.models.fields.CharField', [], {'max_length': '16', 'primary_key': 'True'})
        },
        u'Game.user': {
            'Meta': {'object_name': 'User'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        }
    }

    complete_apps = ['Game']