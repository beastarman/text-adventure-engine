# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import DataMigration
import random
import string
from django.db import models

class Migration(DataMigration):
    HASH_LETTERS = string.ascii_letters+string.digits;

    def forwards(self, orm):
        for i in orm.StateOptions.objects.all():
            i.hash_id = ''.join(random.choice(self.HASH_LETTERS) for _ in range(32))
            i.save()

    def backwards(self, orm):
        "Write your backwards methods here."

    models = {
        u'Game.character': {
            'Meta': {'object_name': 'Character'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pokemon': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Game.Pokemon']"}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Game.State']"})
        },
        u'Game.gameuser': {
            'Meta': {'object_name': 'GameUser'},
            'character': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Game.Character']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Game.User']", 'unique': 'True'})
        },
        u'Game.pokemon': {
            'Meta': {'object_name': 'Pokemon'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pokedex_id': ('django.db.models.fields.IntegerField', [], {})
        },
        u'Game.state': {
            'Meta': {'object_name': 'State'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        u'Game.stateoptions': {
            'Meta': {'object_name': 'StateOptions'},
            'hash_id': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'help': ('django.db.models.fields.TextField', [], {'max_length': '128', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'source': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'targets'", 'to': u"orm['Game.State']"}),
            'target': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'sources'", 'to': u"orm['Game.State']"}),
            'test': ('django.db.models.fields.CharField', [], {'default': "'True'", 'max_length': '128'})
        },
        u'Game.user': {
            'Meta': {'object_name': 'User'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        }
    }

    complete_apps = ['Game']
    symmetrical = True
