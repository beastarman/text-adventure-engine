# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'State'
        db.create_table(u'Game_state', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('text', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'Game', ['State'])

        # Adding model 'StateOptions'
        db.create_table(u'Game_stateoptions', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('source', self.gf('django.db.models.fields.related.ForeignKey')(related_name='targets', to=orm['Game.State'])),
            ('test', self.gf('django.db.models.fields.CharField')(default='True', max_length=128)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('help', self.gf('django.db.models.fields.TextField')(max_length=128, blank=True)),
            ('target', self.gf('django.db.models.fields.related.ForeignKey')(related_name='sources', to=orm['Game.State'])),
        ))
        db.send_create_signal(u'Game', ['StateOptions'])

        # Adding model 'Pokemon'
        db.create_table(u'Game_pokemon', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('pokedex_id', self.gf('django.db.models.fields.IntegerField')()),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=32)),
        ))
        db.send_create_signal(u'Game', ['Pokemon'])

        # Adding model 'Character'
        db.create_table(u'Game_character', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('pokemon', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Game.Pokemon'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=32)),
        ))
        db.send_create_signal(u'Game', ['Character'])

        # Adding model 'User'
        db.create_table(u'Game_user', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('last_login', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('email', self.gf('django.db.models.fields.EmailField')(unique=True, max_length=75)),
            ('is_admin', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'Game', ['User'])


    def backwards(self, orm):
        # Deleting model 'State'
        db.delete_table(u'Game_state')

        # Deleting model 'StateOptions'
        db.delete_table(u'Game_stateoptions')

        # Deleting model 'Pokemon'
        db.delete_table(u'Game_pokemon')

        # Deleting model 'Character'
        db.delete_table(u'Game_character')

        # Deleting model 'User'
        db.delete_table(u'Game_user')


    models = {
        u'Game.character': {
            'Meta': {'object_name': 'Character'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pokemon': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Game.Pokemon']"})
        },
        u'Game.pokemon': {
            'Meta': {'object_name': 'Pokemon'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pokedex_id': ('django.db.models.fields.IntegerField', [], {})
        },
        u'Game.state': {
            'Meta': {'object_name': 'State'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        u'Game.stateoptions': {
            'Meta': {'object_name': 'StateOptions'},
            'help': ('django.db.models.fields.TextField', [], {'max_length': '128', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'source': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'targets'", 'to': u"orm['Game.State']"}),
            'target': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'sources'", 'to': u"orm['Game.State']"}),
            'test': ('django.db.models.fields.CharField', [], {'default': "'True'", 'max_length': '128'})
        },
        u'Game.user': {
            'Meta': {'object_name': 'User'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        }
    }

    complete_apps = ['Game']