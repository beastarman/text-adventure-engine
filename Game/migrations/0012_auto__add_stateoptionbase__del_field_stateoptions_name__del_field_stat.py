# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'StateOptionBase'
        db.create_table(u'Game_stateoptionbase', (
            ('source', self.gf('django.db.models.fields.related.ForeignKey')(related_name='targets', to=orm['Game.State'])),
            ('test', self.gf('django.db.models.fields.CharField')(default='True', max_length=512)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('help', self.gf('django.db.models.fields.TextField')(max_length=128, blank=True)),
            ('hash_id', self.gf('django.db.models.fields.CharField')(max_length=64, primary_key=True)),
        ))
        db.send_create_signal(u'Game', ['StateOptionBase'])

        # Deleting field 'StateOptions.name'
        db.delete_column(u'Game_stateoptions', 'name')

        # Deleting field 'StateOptions.source'
        db.delete_column(u'Game_stateoptions', 'source_id')

        # Deleting field 'StateOptions.test'
        db.delete_column(u'Game_stateoptions', 'test')

        # Deleting field 'StateOptions.hash_id'
        db.delete_column(u'Game_stateoptions', 'hash_id')

        # Deleting field 'StateOptions.help'
        db.delete_column(u'Game_stateoptions', 'help')

        # Adding field 'StateOptions.stateoptionbase_ptr'
        db.add_column(u'Game_stateoptions', u'stateoptionbase_ptr',
                      self.gf('django.db.models.fields.related.OneToOneField')(default=1, to=orm['Game.StateOptionBase'], unique=True, primary_key=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'StateOptionBase'
        db.delete_table(u'Game_stateoptionbase')


        # User chose to not deal with backwards NULL issues for 'StateOptions.name'
        raise RuntimeError("Cannot reverse this migration. 'StateOptions.name' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'StateOptions.name'
        db.add_column(u'Game_stateoptions', 'name',
                      self.gf('django.db.models.fields.CharField')(max_length=32),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'StateOptions.source'
        raise RuntimeError("Cannot reverse this migration. 'StateOptions.source' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'StateOptions.source'
        db.add_column(u'Game_stateoptions', 'source',
                      self.gf('django.db.models.fields.related.ForeignKey')(related_name='targets', to=orm['Game.State']),
                      keep_default=False)

        # Adding field 'StateOptions.test'
        db.add_column(u'Game_stateoptions', 'test',
                      self.gf('django.db.models.fields.CharField')(default='True', max_length=512),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'StateOptions.hash_id'
        raise RuntimeError("Cannot reverse this migration. 'StateOptions.hash_id' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'StateOptions.hash_id'
        db.add_column(u'Game_stateoptions', 'hash_id',
                      self.gf('django.db.models.fields.CharField')(max_length=64, primary_key=True),
                      keep_default=False)

        # Adding field 'StateOptions.help'
        db.add_column(u'Game_stateoptions', 'help',
                      self.gf('django.db.models.fields.TextField')(default='', max_length=128, blank=True),
                      keep_default=False)

        # Deleting field 'StateOptions.stateoptionbase_ptr'
        db.delete_column(u'Game_stateoptions', u'stateoptionbase_ptr_id')


    models = {
        u'Game.character': {
            'Meta': {'object_name': 'Character'},
            'events': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['Game.Event']", 'symmetrical': 'False'}),
            'hp': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'maxHP': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pokemon': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Game.Pokemon']"}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Game.State']"})
        },
        u'Game.event': {
            'Meta': {'object_name': 'Event'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '16', 'primary_key': 'True'})
        },
        u'Game.gameuser': {
            'Meta': {'object_name': 'GameUser'},
            'character': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Game.Character']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Game.User']", 'unique': 'True'})
        },
        u'Game.pokemon': {
            'Meta': {'object_name': 'Pokemon'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pokedex_id': ('django.db.models.fields.IntegerField', [], {})
        },
        u'Game.state': {
            'Meta': {'object_name': 'State'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['Game.Tag']", 'symmetrical': 'False', 'blank': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        u'Game.stateoptionbase': {
            'Meta': {'object_name': 'StateOptionBase'},
            'hash_id': ('django.db.models.fields.CharField', [], {'max_length': '64', 'primary_key': 'True'}),
            'help': ('django.db.models.fields.TextField', [], {'max_length': '128', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'source': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'targets'", 'to': u"orm['Game.State']"}),
            'test': ('django.db.models.fields.CharField', [], {'default': "'True'", 'max_length': '512'})
        },
        u'Game.stateoptions': {
            'Meta': {'object_name': 'StateOptions', '_ormbases': [u'Game.StateOptionBase']},
            u'stateoptionbase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Game.StateOptionBase']", 'unique': 'True', 'primary_key': 'True'}),
            'target': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'sources'", 'to': u"orm['Game.State']"})
        },
        u'Game.tag': {
            'Meta': {'object_name': 'Tag'},
            'tag': ('django.db.models.fields.CharField', [], {'max_length': '16', 'primary_key': 'True'})
        },
        u'Game.user': {
            'Meta': {'object_name': 'User'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        }
    }

    complete_apps = ['Game']