# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'RandomStateOptionSlice.stateoption_ptr'
        db.delete_column(u'Game_randomstateoptionslice', u'stateoption_ptr_id')

        # Adding field 'RandomStateOptionSlice.id'
        db.add_column(u'Game_randomstateoptionslice', u'id',
                      self.gf('django.db.models.fields.AutoField')(default=1, primary_key=True),
                      keep_default=False)

        # Adding field 'RandomStateOptionSlice.test'
        db.add_column(u'Game_randomstateoptionslice', 'test',
                      self.gf('django.db.models.fields.CharField')(default='True', max_length=512),
                      keep_default=False)

        # Adding field 'RandomStateOptionSlice.target'
        db.add_column(u'Game_randomstateoptionslice', 'target',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, related_name='random_sources', to=orm['Game.State']),
                      keep_default=False)

        # Adding field 'RandomStateOptionSlice.random_scene_option'
        db.add_column(u'Game_randomstateoptionslice', 'random_scene_option',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['Game.RandomStateOption']),
                      keep_default=False)


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'RandomStateOptionSlice.stateoption_ptr'
        raise RuntimeError("Cannot reverse this migration. 'RandomStateOptionSlice.stateoption_ptr' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'RandomStateOptionSlice.stateoption_ptr'
        db.add_column(u'Game_randomstateoptionslice', u'stateoption_ptr',
                      self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Game.StateOption'], unique=True, primary_key=True),
                      keep_default=False)

        # Deleting field 'RandomStateOptionSlice.id'
        db.delete_column(u'Game_randomstateoptionslice', u'id')

        # Deleting field 'RandomStateOptionSlice.test'
        db.delete_column(u'Game_randomstateoptionslice', 'test')

        # Deleting field 'RandomStateOptionSlice.target'
        db.delete_column(u'Game_randomstateoptionslice', 'target_id')

        # Deleting field 'RandomStateOptionSlice.random_scene_option'
        db.delete_column(u'Game_randomstateoptionslice', 'random_scene_option_id')


    models = {
        u'Game.character': {
            'Meta': {'object_name': 'Character'},
            'events': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['Game.Event']", 'symmetrical': 'False'}),
            'hp': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'maxHP': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pokemon': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Game.Pokemon']"}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Game.State']"})
        },
        u'Game.event': {
            'Meta': {'object_name': 'Event'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '16', 'primary_key': 'True'})
        },
        u'Game.gameuser': {
            'Meta': {'object_name': 'GameUser'},
            'character': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Game.Character']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Game.User']", 'unique': 'True'})
        },
        u'Game.pokemon': {
            'Meta': {'object_name': 'Pokemon'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pokedex_id': ('django.db.models.fields.IntegerField', [], {})
        },
        u'Game.randomstateoption': {
            'Meta': {'object_name': 'RandomStateOption', '_ormbases': [u'Game.StateOptionBase']},
            u'stateoptionbase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Game.StateOptionBase']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'Game.randomstateoptionslice': {
            'Meta': {'object_name': 'RandomStateOptionSlice'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'random_scene_option': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Game.RandomStateOption']"}),
            'slice': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'target': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'random_sources'", 'to': u"orm['Game.State']"}),
            'test': ('django.db.models.fields.CharField', [], {'default': "'True'", 'max_length': '512'})
        },
        u'Game.state': {
            'Meta': {'object_name': 'State'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['Game.Tag']", 'symmetrical': 'False', 'blank': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        u'Game.stateoption': {
            'Meta': {'object_name': 'StateOption', '_ormbases': [u'Game.StateOptionBase']},
            u'stateoptionbase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Game.StateOptionBase']", 'unique': 'True', 'primary_key': 'True'}),
            'target': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'sources'", 'to': u"orm['Game.State']"})
        },
        u'Game.stateoptionbase': {
            'Meta': {'object_name': 'StateOptionBase'},
            'hash_id': ('django.db.models.fields.CharField', [], {'max_length': '64', 'primary_key': 'True'}),
            'help': ('django.db.models.fields.TextField', [], {'max_length': '128', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'source': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'targets'", 'to': u"orm['Game.State']"}),
            'test': ('django.db.models.fields.CharField', [], {'default': "'True'", 'max_length': '512'})
        },
        u'Game.tag': {
            'Meta': {'object_name': 'Tag'},
            'tag': ('django.db.models.fields.CharField', [], {'max_length': '16', 'primary_key': 'True'})
        },
        u'Game.user': {
            'Meta': {'object_name': 'User'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        }
    }

    complete_apps = ['Game']