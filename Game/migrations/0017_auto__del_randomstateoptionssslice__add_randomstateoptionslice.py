# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'RandomStateOptionssSlice'
        db.delete_table(u'Game_randomstateoptionssslice')

        # Adding model 'RandomStateOptionSlice'
        db.create_table(u'Game_randomstateoptionslice', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('test', self.gf('django.db.models.fields.CharField')(default='True', max_length=512)),
            ('target', self.gf('django.db.models.fields.related.ForeignKey')(related_name='random_sources', to=orm['Game.State'])),
            ('slice', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('random_scene_option', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Game.RandomStateOption'])),
        ))
        db.send_create_signal(u'Game', ['RandomStateOptionSlice'])


    def backwards(self, orm):
        # Adding model 'RandomStateOptionssSlice'
        db.create_table(u'Game_randomstateoptionssslice', (
            ('slice', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('target', self.gf('django.db.models.fields.related.ForeignKey')(related_name='random_sources', to=orm['Game.State'])),
            ('random_scene_option', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Game.RandomStateOption'])),
            ('test', self.gf('django.db.models.fields.CharField')(default='True', max_length=512)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'Game', ['RandomStateOptionssSlice'])

        # Deleting model 'RandomStateOptionSlice'
        db.delete_table(u'Game_randomstateoptionslice')


    models = {
        u'Game.character': {
            'Meta': {'object_name': 'Character'},
            'events': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['Game.Event']", 'symmetrical': 'False'}),
            'hp': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'maxHP': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pokemon': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Game.Pokemon']"}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Game.State']"})
        },
        u'Game.event': {
            'Meta': {'object_name': 'Event'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '16', 'primary_key': 'True'})
        },
        u'Game.gameuser': {
            'Meta': {'object_name': 'GameUser'},
            'character': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Game.Character']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Game.User']", 'unique': 'True'})
        },
        u'Game.pokemon': {
            'Meta': {'object_name': 'Pokemon'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'pokedex_id': ('django.db.models.fields.IntegerField', [], {})
        },
        u'Game.randomstateoption': {
            'Meta': {'object_name': 'RandomStateOption', '_ormbases': [u'Game.StateOptionBase']},
            u'stateoptionbase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Game.StateOptionBase']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'Game.randomstateoptionslice': {
            'Meta': {'object_name': 'RandomStateOptionSlice'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'random_scene_option': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['Game.RandomStateOption']"}),
            'slice': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'target': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'random_sources'", 'to': u"orm['Game.State']"}),
            'test': ('django.db.models.fields.CharField', [], {'default': "'True'", 'max_length': '512'})
        },
        u'Game.state': {
            'Meta': {'object_name': 'State'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['Game.Tag']", 'symmetrical': 'False', 'blank': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        u'Game.stateoption': {
            'Meta': {'object_name': 'StateOption', '_ormbases': [u'Game.StateOptionBase']},
            u'stateoptionbase_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['Game.StateOptionBase']", 'unique': 'True', 'primary_key': 'True'}),
            'target': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'sources'", 'to': u"orm['Game.State']"})
        },
        u'Game.stateoptionbase': {
            'Meta': {'object_name': 'StateOptionBase'},
            'hash_id': ('django.db.models.fields.CharField', [], {'max_length': '64', 'primary_key': 'True'}),
            'help': ('django.db.models.fields.TextField', [], {'max_length': '128', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'source': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'targets'", 'to': u"orm['Game.State']"}),
            'test': ('django.db.models.fields.CharField', [], {'default': "'True'", 'max_length': '512'})
        },
        u'Game.tag': {
            'Meta': {'object_name': 'Tag'},
            'tag': ('django.db.models.fields.CharField', [], {'max_length': '16', 'primary_key': 'True'})
        },
        u'Game.user': {
            'Meta': {'object_name': 'User'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        }
    }

    complete_apps = ['Game']