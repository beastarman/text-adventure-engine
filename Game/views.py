from django.http import *
from django.views.decorators.csrf import csrf_exempt
from django.template import RequestContext, loader

import json

from Game.controller import *

# Create your views here.
def game(request):
	template = loader.get_template('html/base.html')

	context = RequestContext(request, {})

	return HttpResponse(template.render(context))

def scene(request):
	sceneController = SceneController(request)

	sceneController = sceneController.getSpecialized()

	j = {}

	if(sceneController.status == sceneController.STATUS_NOT_LOGGED_IN):
		j["status"] = "Fatal"
		j["response"] = {}
	else:
		j["status"] = "Success"
		j["response"] = sceneController.asJSON()

	return HttpResponse(json.dumps(j))

@csrf_exempt
def advance(request):
	advanceSceneController = AdvanceSceneController(request,request.POST.get("key",""))

	j = {}

	if(advanceSceneController.status != advanceSceneController.STATUS_NOT_LOGGED_IN):
		advanceSceneController = advanceSceneController.getSpecialized()

		advanceSceneController.advance();

	return scene(request)

