from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.db import models

from django.template import Template, Context

from Game.state_tests.tests import eval_dict
from Game.state_tests.execs import eval_dict as eval_dict_exec

import string
import random

weighted_choice = lambda s : random.choice(sum(([v]*wt for v,wt in s),[])) if len(s) else None
def clamp(n, smallest, largest):
	return max(smallest, min(n, largest))

class State(models.Model):
	text = models.TextField(blank=False,null=False);

	tags = models.ManyToManyField("Tag", blank=True);

	arrivalExec = models.TextField(blank=True, help_text="<br/>".join(eval_dict.keys() + eval_dict_exec.keys()));

	def getText(self,gameuser):
		return Template(self.text).render(Context({"user":gameuser,"character":gameuser.character}))

	def execTask(self,gameuser):
		d = dict.copy(eval_dict)
		d.update(eval_dict_exec)

		d.update({"user":gameuser,"character":gameuser.character})

		if(self.arrivalExec):
			exec(self.arrivalExec,d)

	def __unicode__(self):
		return "%d. %s" % (self.id,self.text[:50]);

class BattleState(State):
	enemy = models.ForeignKey("Character")

	def __unicode__(self):
		return "%d. Battle against %s" % (self.id,self.enemy.name);

class StateOptionBase(models.Model):
	HASH_LETTERS = string.ascii_letters+string.digits;

	hash_id = models.CharField(max_length=64, blank=True, primary_key=True)

	source = models.ForeignKey(State, related_name="targets")
	test = models.CharField(max_length=512, default="True", help_text="<br/>".join(eval_dict.keys()))
	name = models.CharField(max_length=32)
	help = models.TextField(max_length=128, blank=True)

	def testOption(self,gameuser):
		d = dict.copy(eval_dict)

		d.update({"user":gameuser,"character":gameuser.character})

		return eval(self.test,d)

	def execTask(self,gameuser):
		pass

	def getTarget(self,gameuser):
		return None

	def getSpecialized(self):
		if(self.__class__ != StateOptionBase):
			return self;

		attrs = ["stateoption","randomstateoption","specialstateoption"]

		for attr in attrs:
			try:
				return self.__getattribute__(attr).getSpecialized()
			except Exception:
				pass

		return self

	def save(self,*args,**kwargs):
		if(not self.hash_id):
			self.hash_id = ''.join(random.choice(self.HASH_LETTERS) for _ in range(32))

		super(StateOptionBase,self).save(*args,**kwargs)

	def __unicode__(self):
		return "%s: from [%s]" % (self.name, self.source.__unicode__()[:35])

class StateOption(StateOptionBase):
	target = models.ForeignKey(State, related_name="sources")
	transactionExec = models.TextField(blank=True, help_text="<br/>".join(eval_dict.keys() + eval_dict_exec.keys()));

	def getTarget(self,gameuser):
		return self.target

	def execTask(self,gameuser):
		d = dict.copy(eval_dict)
		d.update(eval_dict_exec)

		d.update({"user":gameuser,"character":gameuser.character})

		if(self.transactionExec):
			exec(self.transactionExec,d)

	def __unicode__(self):
		return "%s to [%s]" % (super(StateOption,self).__unicode__(), self.target.__unicode__()[:35])

class RandomStateOption(StateOptionBase):

	def testOption(self,gameuser):
		return super(RandomStateOption,self).testOption(gameuser) and len(self.__getAvailableSlices(gameuser))>0

	def getTarget(self,gameuser):
		return weighted_choice([(i.target,i.slice) for i in self.__getAvailableSlices(gameuser)])

	def __getAvailableSlices(self,gameuser):
		return [i for i in self.randomstateoptionslice_set.all() if i.testSlice(gameuser)]

class RandomStateOptionSlice(models.Model):
	test = models.CharField(max_length=512, default="True", help_text="<br/>".join(eval_dict.keys()))
	target = models.ForeignKey(State, related_name="random_sources")

	slice = models.IntegerField(default=1)

	random_scene_option = models.ForeignKey(RandomStateOption,null=False,blank=False)

	def testSlice(self,gameuser):
		d = dict.copy(eval_dict)

		d.update({"user":gameuser,"character":gameuser.character})

		return eval(self.test,d)

	def __unicode__(self):
		return "Go to [%s] if %d" % (self.target.__unicode__()[:35], self.slice)

class Species(models.Model):
	encyclopedia_id = models.IntegerField()
	name = models.CharField(max_length=32)

	def __unicode__(self):
		return "%d. %s (%d)" % (self.id,self.name, self.encyclopedia_id);

class Character(models.Model):
	name = models.CharField(max_length=32)

	maxHP = models.IntegerField(default=100)
	hp = models.IntegerField(default=100)

	maxLust = models.IntegerField(default=100)
	lust = models.IntegerField(default=0)

	attack = models.IntegerField(default=10)
	defense = models.IntegerField(default=10)

	def getRandomCharacterAction(self,gameuser,source,target):
		return weighted_choice([(i,i.chances) for i in self.characteraction_set.all() if i.testAction(gameuser,source,target)])

	def save(self, *args, **kwargs):
		self.hp = clamp(self.hp,0,self.maxHP)
		self.lust = clamp(self.lust,0,self.maxLust)

		super(Character, self).save(*args,**kwargs)

	def __unicode__(self):
		return "%d. %s" % (self.id,self.name);

class EnemyState(models.Model):
	character = models.ForeignKey("Character")

	hp = models.IntegerField(default=100)

	lust = models.IntegerField(default=0)

	lastAction = models.ForeignKey("CharacterAction",blank=True,null=True)

class Manager(BaseUserManager):
	def create_user(self,email,password,currency=0):
		if not (email and password):
			raise ValueError('Users must have an email address')

		user = User(email=email)
		user.preffered_currency = currency
		user.set_password(password)
		user.save()

		return user

	def create_superuser(self, email, password,currency=0):
		user = self.create_user(email,password,currency)
		user.is_admin=True
		user.save()

		return user

class User(AbstractBaseUser):
	email = models.EmailField(unique=True)

	objects = Manager()

	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = []

	is_admin = models.BooleanField(default=False)

	def __unicode__(self):
		return self.email

	def get_full_name(self):
		return self.email

	def get_short_name(self):
		return self.email

	def has_perm(self, perm, obj=None):
		return True

	def has_module_perms(self, app_label):
		return True

	@property
	def is_staff(self):
		return self.is_admin

	def __unicode__(self):
		return self.email

class GameUser(models.Model):
	user = models.OneToOneField(User)
	character = models.ForeignKey(Character)

	species = models.ForeignKey(Species)
	state = models.ForeignKey(State)
	events = models.ManyToManyField("Event", blank=True);

	enemyState = models.ForeignKey(EnemyState,blank=True,null=True)

	lastHub = models.ForeignKey(State,related_name="usersInHub")

	def __unicode__(self):
		return "Character %s from User %s" % (self.character.__unicode__(), self.user.__unicode__())

class Event(models.Model):
	key = models.CharField(primary_key=True, max_length=16, blank=False)
	description = models.CharField(max_length=100, blank=True)

	def __unicode__(self):
		return "%s: %s" % (self.key, self.description)

class Tag(models.Model):
	tag = models.CharField(primary_key=True, max_length=16, blank=False)

	def __unicode__(self):
		return self.tag;

class GlobalSettings(models.Model):
	HUB_WORLD_TAG = "hubWorldTag";
	DEFAULT_HUB = "defaultHub"

	key = models.CharField(max_length=16, primary_key=True, choices=[
		(HUB_WORLD_TAG, "Hug World Tag"),
		(DEFAULT_HUB, "Default Hub State"),
	])
	value = models.CharField(max_length=64)

	def __unicode__(self):
		return "%s: %s" % (self.get_key_display(), self.value)

class CharacterAction(models.Model):
	HASH_LETTERS = string.ascii_letters+string.digits;

	hash_id = models.CharField(max_length=64, blank=True, primary_key=True)

	name = models.CharField(max_length=32)
	description = models.TextField()

	actionTest = models.CharField(max_length=512, default="True", help_text="<br/>".join(eval_dict.keys()))
	actionExec = models.TextField(blank=True, help_text="<br/>".join(eval_dict.keys() + eval_dict_exec.keys()))

	chances = models.IntegerField(default=1)

	character = models.ForeignKey(Character)

	def getDescription(self,gameuser,source,target):
		return Template(self.description).render(Context({"user":gameuser,"character":gameuser.character,"source":source,"target":target}))

	def testAction(self,gameuser,source,target):
		d = dict.copy(eval_dict)

		d.update({"user":gameuser,"character":gameuser.character,"source":source,"target":target})

		return eval(self.actionTest,d)

	def execTask(self,gameuser,source,target):
		d = dict.copy(eval_dict)
		d.update(eval_dict_exec)

		d.update({"user":gameuser,"character":gameuser.character,"source":source,"target":target})

		if(self.actionExec):
			exec(self.actionExec,d)

	def save(self,*args,**kwargs):
		if(not self.hash_id):
			self.hash_id = "CharacterAction_" + ''.join(random.choice(self.HASH_LETTERS) for _ in range(32))

		super(CharacterAction,self).save(*args,**kwargs)

class SpecialStateOption(StateOptionBase):
	stateExec = models.TextField(help_text="Use retVal.append(<return>) to return state<br/><br/>" + "<br/>".join(eval_dict.keys() + eval_dict_exec.keys()))

	def getTarget(self,gameuser):
		retVal = []

		d = dict.copy(eval_dict)
		d.update(eval_dict_exec)
		d.update({"user":gameuser,"character":gameuser.character,"retVal":retVal})

		exec(self.stateExec,d)

		return retVal[0];