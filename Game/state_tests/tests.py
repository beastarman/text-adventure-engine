__author__ = 'beastarman'

def event_has_triggered(gameuser,event_key):
    return gameuser.events.filter(key=event_key).exists()

def is_current_battle_won(gameuser):
	return gameuser.enemyState.hp<=0

def is_current_battle_lost(gameuser):
	return gameuser.character.hp<=0

eval_dict =dict([(i.__name__,i) for i in [
    event_has_triggered,
	is_current_battle_won,
	is_current_battle_lost,
]])