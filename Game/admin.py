from django.contrib import admin
from Game.models import *

# Register your models here.
admin.site.register(State)
admin.site.register(BattleState)
admin.site.register(EnemyState)
admin.site.register(StateOption)
admin.site.register(RandomStateOption)
admin.site.register(RandomStateOptionSlice)
admin.site.register(User)
admin.site.register(GameUser)
admin.site.register(Species)
admin.site.register(Character)
admin.site.register(Event)
admin.site.register(Tag)
admin.site.register(GlobalSettings)
admin.site.register(CharacterAction)
admin.site.register(SpecialStateOption)